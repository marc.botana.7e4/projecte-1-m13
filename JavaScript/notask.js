/**
 * Función que añade una lista.
 */
function addList() {
    var listName = prompt("Nombre de la lista", "Nueva lista"); //default: nombre por defecto
    if (listName != null) {     //Aquí va el codigo que se ejecuta después de introducir el nombre de la lista.
        var container = document.createElement("li")
        var tag = document.createElement("p")
        var text = document.createTextNode(listName)
        tag.appendChild(text)
        var listas = document.getElementsByClassName("listas")[0]
        listas.appendChild(container)
        container.appendChild(tag)
        alert("Tarea añadida correctamente.")
    }
}

/**
 * Función que muestra las tareas actuales.
 */
function showTasks(){
    var Storage_size = localStorage.length;

    if (Storage_size > 0){
        for (var i = 0; i < Storage_size; i++){
            var key = localStorage.key(i);

            if(key.indexOf(Mask) === 0){
                $('<li></li>').addClass('task').attr('data-itemid', key).text(localStorage.getItem(key)).appendTo(List);
            }
        }
    }
}






/*
* $('.tdlApp input').on('keydown',function(e) {
            if (e.keyCode !== 13) return; // keycode = 13 -> tecla ENTER
            var str = e.target.value;
            e.target.value = "";

            if (str.length > 0){
                var number_Id = 0;
                List.children().each(function (index, el){
                    var element_Id = $(el).attr('data-itemid').slice(4); // La función slice crea una copia del array en el index 4.
                    if (element_Id > number_Id)
                        number_Id = element_Id;
                })
                number_Id++;
                localStorage.setItem(Mask+number_Id, str);
                $('<li></li>').addClass('task').attr('data-itemid', Mask+number_Id).text(str).appendTo(List);
            }
        })
* */